## Android WebView with a full functions
Front-end of Kinscape website

## Preview

![image](https://drive.google.com/uc?export=view&id=1rmuiZxCim84WyxLfE0CTSCmPemUJZbXm)
## Run
```sh
git clone https://gitlab.com/superdev1008_xecure/kinscape.git
cd kinscape
./gradlew build